package com.CasaPiu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CasaPiuApplication {

	public static void main(String[] args) {
		SpringApplication.run(CasaPiuApplication.class, args);
		
	}
}
