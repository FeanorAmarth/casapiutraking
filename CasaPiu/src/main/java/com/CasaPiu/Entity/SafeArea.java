package com.CasaPiu.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/* potrebbero mancare alcuni parametri non stettamente necessari alla gestione della posizione.*/

@Entity
@Table(name = "SafeArea")
public class SafeArea {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	
	//@Column(columnDefinition = "geometry")
	@Column(columnDefinition = "text")
	private String SArea;
	
	private String name;
	
	public SafeArea() {
		
	}
	
	public int getid() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSArea() {
		return SArea;
	}
	
	public void setId(int Id) {
		id=Id;
	}
	
	public void setName(String Name) {
		name=Name;
	}
	
	public void setSArea(String Area) {
		SArea=Area;
	}
	
}
