package com.CasaPiu.Repository;

import org.springframework.data.repository.CrudRepository;

import com.CasaPiu.Entity.UtenteEdu;

public interface RUtenteEdu extends CrudRepository<UtenteEdu,Long>{

}