package com.CasaPiu.Entity;

import java.util.Set;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;

/* potrebbero mancare alcuni parametri non stettamente necessari alla gestione della posizione,
 * attualmente � un entit� inutilizzata...*/

@Entity
@Table(name = "UtenteEdu")

public class UtenteEdu {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	
	private String nome;
	
	private String username;
	
	private String telefono;	
	
	private int utype;
	
	@Autowired
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Set<UtenteCtrl> ctrl;
}