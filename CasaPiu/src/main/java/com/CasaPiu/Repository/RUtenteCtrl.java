package com.CasaPiu.Repository;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.CasaPiu.Entity.UtenteCtrl;

public interface RUtenteCtrl extends CrudRepository<UtenteCtrl,Long>{
	
	@Autowired
	UtenteCtrl findById(int id);
	
	@Autowired
	UtenteCtrl findByPhone(String phone);
	
	/* sfruttando spatial mysql si verifica che vi sia un intersezione tra: le aree sicure specificate (sarea � in generale un MULTIPOLYGON), 
	 * ed un buffer circolare (con un raggio di circa 5m) centrato nella posizione dell'utente controllato*/
	@Query(value="SELECT ST_Intersects( ST_GeomFromText( ?1 ),ST_Buffer(ST_GeomFromText(?2),0.00005) ) as isinsarea", nativeQuery=true)
	int getIsInSArea(String sarea, String PosCrtl);

}