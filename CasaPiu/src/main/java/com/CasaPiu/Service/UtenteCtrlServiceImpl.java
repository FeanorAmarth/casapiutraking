package com.CasaPiu.Service;

import java.util.Iterator;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CasaPiu.Entity.SafeArea;
import com.CasaPiu.Entity.SafePath;
import com.CasaPiu.Entity.UtenteCtrl;
import com.CasaPiu.Repository.RUtenteCtrl;

@Service("UtenteCtrlService")
public class UtenteCtrlServiceImpl implements UtenteCtrlService{
	
	@Autowired
	private RUtenteCtrl Uctrl;

	@Override
	public UtenteCtrl GetUserCtrlById(int id) {
		return Uctrl.findById(id);
	}
	
	@Override
	public UtenteCtrl GetUserCtrlByPhone(String phone) {
		return Uctrl.findByPhone(phone);
	}

	@Override
	public int IsInSArea(String sarea, String PosCrtl) {
		return Uctrl.getIsInSArea(sarea, PosCrtl);
	}

	@Override
	public String GetMultiPolyWKT(UtenteCtrl uctrl) {
		
		String multipoly="MULTIPOLYGON(";
		Set<SafeArea> SFA=uctrl.getSarea();
		
		if(!SFA.isEmpty()) {
			Iterator<SafeArea> it=SFA.iterator();
			for(int i=0;i<SFA.size();i++) {
				String tmp=it.next().getSArea();
				if(i!=SFA.size()-1) {
					multipoly+=tmp.substring(7, tmp.length())+",";
				}else {
					multipoly+=tmp.substring(7, tmp.length())+")";
				}
			}
		}else {
			multipoly="";
		}
		
		return multipoly;
	}
	
	@Override
	public SafePath GetMySPathById(UtenteCtrl uctrl, int id) {
		
		Set<SafePath> SFP=uctrl.getSpath();
		SafePath path=null;
		
		if(!SFP.isEmpty()){
			Iterator<SafePath> it=SFP.iterator();
			for(int i=0;i<SFP.size();i++) {
				path=it.next();
				if(path.getId()==id)
					break;
			}
		}
		
		return path;
	}

	@Override
	@Transactional
	public void SaveUserCtrl(UtenteCtrl uctrl) {
		Uctrl.save(uctrl);
	}
	
	

}
