package com.CasaPiu.Entity;

import java.util.Set;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnore;

/* essenzialmente una entity rapresentazione dell'utente controllato, 
 * potrebbero mancare alcuni parametri non stettamente necessari alla gestione della posizione.*/

@Entity
@Table(name = "UtenteCtrl")
public class UtenteCtrl {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	
	private String phone;
	
	private String name;
	
	private String sname;
	
	private int alert;
	
	@Autowired
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "uctrl_edu", joinColumns = @JoinColumn(name = "uctrl_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "edu_id", referencedColumnName = "id"))
	private Set<UtenteEdu> edu;
	
	@Autowired
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "uctrl_area", joinColumns = @JoinColumn(name = "uctrl_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "area_id", referencedColumnName = "id"))
	private Set<SafeArea> sarea;
	
	@Autowired
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "uctrl_path", joinColumns = @JoinColumn(name = "uctrl_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "path_id", referencedColumnName = "id"))
	private Set<SafePath> spath;	
	
	public UtenteCtrl() {
		
	}
	
	public UtenteCtrl(int id, String Name,String Sname, int Alert) {
		this.id=id;
		this.name=Name;
		this.sname=Sname;
		this.alert=Alert;
	}
	
	public String getNome() {
		return name;
	}
	
	@JsonIgnore
	public String getPhone() {
		return phone;
	}
	
	public String getSnome() {
		return sname;
	}
	
	public int getAlert() {
		return alert;
	}
	
	@JsonIgnore
	public Set<SafeArea> getSarea(){
		return sarea;
	}
	@JsonIgnore
	public Set<SafePath> getSpath(){
		return spath;
	}
	
	public void setAlert(int alert) {
		this.alert=alert;
	}
	
	public void setPhone(String phone) {
		this.phone=phone;
	}

}