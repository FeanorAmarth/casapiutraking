package com.CasaPiu.Service;

import com.CasaPiu.Entity.SafePath;
import com.CasaPiu.Entity.UtenteCtrl;

public interface UtenteCtrlService {
	
	/* restituisce l'utente controllato con l'id specificato*/
	public UtenteCtrl GetUserCtrlById(int id);
	
	/* restituisce l'utente controllato con il numero di telefono specificato*/
	public UtenteCtrl GetUserCtrlByPhone(String phone);
	
	/* verifica che la posizione dell'utente da controllare sia all'interno di un'area sicura*/
	public int IsInSArea(String sarea, String PosCrtl);
	
	/* restituisce una stringa (secondo il formato WKT) che identifica l'isieme dei poligoni (MULTIPOLYGON)
	 * che rappresentanti le aree sicure dell'utente controllato*/
	public String GetMultiPolyWKT(UtenteCtrl uctrl);
	
	/*restituisce il percorso sicuro assegnato(id) all'utente controllato*/
	public SafePath GetMySPathById(UtenteCtrl uctrl, int id);
	
	/*salva su db l'utente controllato specificato*/
	public void SaveUserCtrl(UtenteCtrl uctrl);
	
}

