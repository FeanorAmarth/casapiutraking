/* Prima Bozza del Controller, che reimplementa l'interfaccia offerta al client Android.
 * Al momento vengono gestite le seguenti attivit� dell'app;
 * - Navigazione Libera
 * - Navigazione sul Percorso
 * - Creazione di un POI durante la navigazione
 * ho cercato di mantenere invariate la forma dei JSON di risposta, e sembra che l'app
 * riesca ad interfacciarsi con questo server senza grossi cambiamenti....*/

package com.CasaPiu.Controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.CasaPiu.Entity.UtenteCtrl;
import com.CasaPiu.Entity.PointOfInterest;
import com.CasaPiu.Entity.SafePath;
import com.CasaPiu.Service.PointOfInterestService;
import com.CasaPiu.Service.SafePathService;
import com.CasaPiu.Service.UtenteCtrlService;

@Controller
@RequestMapping("/OutDtrack")
public class COutDoorTracking {
	
	@Autowired
	private UtenteCtrlService UCservice;
	
	@Autowired
	private SafePathService SPservice;
	
	@Autowired
	private PointOfInterestService POIservice;
	
	/*controlla che l'utente identificato dal numero di telefono "phone",
	 * sia all'intero di una delle sue aree sicure durante la navigazione libera.
	 * Restitusce un JSON che notifica il suo stato;
	 * -alert : 0 � al sicuro (nessun allerme)
	 * -alert : 1 � all'esterno di tutte le aree a lui associate (induce allarme nel dispositivo mobile)*/
	
	@GetMapping(params  = {"task=monitor" , "phone", "lat", "lon"}, produces="application/json")
	public @ResponseBody String whereAreYou(@RequestParam("lat") double lat, @RequestParam("lon") double lon, @RequestParam("phone") String  phone){
		
		String PosWKT="POINT("+lat+" "+lon+")";
		UtenteCtrl imctrl=UCservice.GetUserCtrlByPhone(phone);
		String Res;
		
		if(imctrl!=null) {
			if(UCservice.IsInSArea(UCservice.GetMultiPolyWKT(imctrl), PosWKT)==1) {
				imctrl.setAlert(0);
				System.out.println("in SArea");
			}else {
				imctrl.setAlert(1);
				System.out.println("out SArea");
			}
			UCservice.SaveUserCtrl(imctrl);
			Res="{ \"alert\" : "+imctrl.getAlert()+" }";
		}else {
			System.out.println("no result");
			Res="{ \"alert\" : 1 }";
		}
		
		return Res;
	}
	
	/* controlla che l'utente identificato dal numero di telefono "phone"
	 * segua il suo percorso sicuro (id), ed aggiorna il POI mostrato.
	 * Restitusce un JSON che notifica il suo stato, ed il POI piu vicino all'attuale posizione dell'utente;
	 * -alert : come sopra
	 * -pois: attributi del POI pi� vicino alla posizione dell'utente lungo il percorso*/
	@GetMapping(params  = {"task=navigazione", "id" , "phone", "lat", "lon"}, produces="application/json")
	public @ResponseBody String whereAreYou(@RequestParam("id") int id, @RequestParam("lat") double lat, @RequestParam("lon") double lon, @RequestParam("phone") String  phone){
		
		String Pos="POINT("+lat+" "+lon+")";
		String Res="{";
		UtenteCtrl imctrl=UCservice.GetUserCtrlByPhone(phone);
		
		if(imctrl!=null) {
			
			SafePath imsp=UCservice.GetMySPathById(imctrl, id);
			String JPOI="1";
			Res+="\"alert\" : "+imctrl.getAlert();
			if(SPservice.IsInSpath( imsp.getSPath(), Pos)==1) {
				imctrl.setAlert(0);
				JPOI=POIservice.getNextPoiJson(id, Pos);
				Res+=", \"pois\" : "+JPOI+"}";
				//System.out.println("in Spath");
			}else {
				imctrl.setAlert(1);
				Res+="}";
				//System.out.println("out Spath");
			}
			UCservice.SaveUserCtrl(imctrl);
			
			
		}else {
			//System.out.println("no result");
			Res+="\"alert\" : 1}";
		}
		
		return Res;
	}
	
	/*restituisce un JSON con la lista dei percorsi associati ad un utente*/
	@GetMapping(params  = {"task=get_percorsi", "phone"}, produces="application/json")
	public @ResponseBody Set<SafePath> WhatAreYourPaths(@RequestParam("phone") String  phone){
		
		UtenteCtrl imctrl=UCservice.GetUserCtrlByPhone(phone);
		
		return imctrl.getSpath();
	}
	
	/* Gestice l'upload di una foto dal dispositivo durante la navigazione (sia libera che in percorso). A differenza degli altri casi,
	 * in questo, ho dovuto aggiuntere un parametro {"task=LoadPhotoPOI"} alla richiesta effettuata dal client...
	 * non sono riuscito a mappare il POST HTTP in altro modo. Tuttavia, nel caso sia necessario, � possibile 
	 * reimplementare il controller del mapping utilizzato di default da Spring MVC...*/
	
	@PostMapping(params  = {"task=LoadPhotoPOI"})
	public @ResponseBody String HandlePoiPhoto(@RequestParam("photo") MultipartFile photo, @RequestParam("lat") double lat, @RequestParam("lon") double lon){
		
		String PhotoPath="C:\\Users\\Beren\\Documents\\workspace-sts-3.9.5.RELEASE\\CasaPiu\\src\\main\\resources\\static\\POImage\\";
		String response ="{\"photo\" : ";
		
		if(!photo.isEmpty()) {
			try {
				String imname=photo.getOriginalFilename();
				File PhotoPoi= new File( PhotoPath + imname );
				BufferedOutputStream stream = new BufferedOutputStream ( new FileOutputStream(PhotoPoi) );
				stream.write(photo.getBytes());
				stream.close();
				response+=0;
				
				PointOfInterest poi = new PointOfInterest(lat, lon, imname);
				POIservice.SavePoi(poi);
				
			}catch (Exception e) {
				response+=1;
			}
		}else{
			response+=1;
		}
		
		System.out.println(response+"}");
		return response+="}";
	}
	
}