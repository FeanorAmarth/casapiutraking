package com.CasaPiu.Entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnore;

/* potrebbero mancare alcuni parametri non stettamente necessari alla gestione della posizione, 
 * ci potrebbero essere metodi get e set ridondanti a causa delle prove per la generazione dei JSON... sar� riordinato.*/

@Entity
@Table(name = "SafePath")
public class SafePath {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	
	@Column(columnDefinition = "text")
	private String spath;
	
	private int duration;
	
	private String name;
	
	@Autowired
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(
				name = "spath_poi", 
				joinColumns = @JoinColumn(name = "spath_id", referencedColumnName = "id"),
				inverseJoinColumns = @JoinColumn(name = "poi_id", referencedColumnName = "id")
				)
	
	private Set<PointOfInterest> poi;
	
	public SafePath() {
		
	}
	
	public int getId() {
		return id;
	}
	
	@JsonIgnore
	public String getName() {
		return name;
	}
	
	@JsonIgnore
	public int getDuration() {
		return duration;
	}
	
	@JsonIgnore
	public String getSPath() {
		return spath;
	}
	
	public String getNome() {
		return name;
	}
	
	public int getTempo() {
		return duration;
	}
	
	public String getThe_geom() {
		return spath;
	}
	
	@JsonIgnore
	public Set<PointOfInterest> getPoi(){
		return poi;
	}
	
}
