package com.CasaPiu.Service;

public interface SafePathService {
	
	/* verifica che la posizione dell'utente da controllare sia all'interno di un percorso sicuro*/
	public int IsInSpath(String spath, String PosCrtl);
	
}
