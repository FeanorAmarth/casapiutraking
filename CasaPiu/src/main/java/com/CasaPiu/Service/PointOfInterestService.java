package com.CasaPiu.Service;

import java.util.Set;

import com.CasaPiu.Entity.PointOfInterest;

public interface PointOfInterestService {
	
	/* restituisce un JSON con l'elenco di tutti i POI specificati. questa funzione � attulemente inutilizzata
	 * perch� frutto di un tentativo di gestione dei punti di interesse fallito... non avevo compreso come il client
	 * gestisse la lista di poi rievuta*/
	String getPoisJson(Set<PointOfInterest> POIs);
	
	/* restituisce il punto di interesse, associato al percorso specificato, pi� vicino all'attuale posizione dell'utente*/
	String getNextPoiJson(int id, String PosCrtl);
	
	/*salva su db il punto d'interesse specificato*/
	void SavePoi(PointOfInterest POI);
	
}
