package com.CasaPiu.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.CasaPiu.Entity.SafeArea;


public interface RSafeArea extends JpaRepository<SafeArea,Long>{
	
	@Autowired
	SafeArea findById(int id);
	
	//@Query(value = "SELECT ST_Intersects( ST_GeomFromText(secure_area.sarea), ST_Buffer(ST_GeomFromText(?2),0.00005) ) as inbound FROM safe_area WHERE id=?1", nativeQuery = true)
	//String getInBoundsById(int id, String PosCtrl);
	
}
