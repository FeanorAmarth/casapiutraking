package com.CasaPiu.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CasaPiu.Repository.RSafePath;

@Service("SafePathService")
public class SafePathServiceImpl implements SafePathService{
	
	@Autowired
	private RSafePath Rspath;
	
	@Override
	public int IsInSpath(String spath, String PosCrtl) {
		return Rspath.getIsInSPath(spath, PosCrtl);
	}

}
