package com.CasaPiu.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.CasaPiu.Entity.PointOfInterest;

public interface RPointOfInterest extends JpaRepository<PointOfInterest,Long>{
	
	@Autowired
	PointOfInterest findById(int id);
	
	/*sfruttando spatial mysql, si cercano i POI associati al percorso specificato (id) che 
	 * siano i pi� vicini alla posizione attuale dell'utente controllato. */
	@Query(value="SELECT point_of_interest.*, ST_Distance(ST_GeomFromText(?2),ST_GeomFromText(point_of_interest.pos)) as distanza " + 
			"FROM point_of_interest " + 
			"INNER JOIN spath_poi ON spath_poi.poi_id=point_of_interest.id " + 
			"WHERE spath_poi.spath_id=?1 ORDER BY distanza LIMIT 1", nativeQuery=true)
	PointOfInterest getNextPoi(int id, String PosCrtl);

}
