package com.CasaPiu.Entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

/* potrebbero mancare alcuni parametri non stettamente necessari alla gestione della posizione.*/

@Entity
@Table(name = "PointOfInterest")
public class PointOfInterest {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private int id;
	
	@Column(length = 50)
	private String pos;
	
	private String descr;
	
	private String rimage;
	
	public PointOfInterest() {
		
	}
	
	public PointOfInterest (double lat, double lon, String imname) {
		this.pos="POINT("+lat+" "+lon+")";
		this.descr="Default Description";
		this.rimage=imname;
	}
	
	public void setId(int ids) {
		id=ids;
	}
	
	public void setPos(String poss) {
		pos=poss;
	}
	
	public void setDescr(String descrs) {
		descr=descrs;
	}
	
	public void setRimage(String rimages) {
		rimage=rimages;
	}
	
	
	public int getId() {
		return id;
	}
	
	public String getPos() {
		return pos;
	}
	
	public String getDescr() {
		return descr;
	}
	
	public String getRimage() {
		return rimage;
	}
	
}
