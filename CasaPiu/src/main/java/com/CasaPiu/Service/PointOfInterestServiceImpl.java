package com.CasaPiu.Service;

import java.util.Iterator;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CasaPiu.Entity.PointOfInterest;
import com.CasaPiu.Repository.RPointOfInterest;

@Service("PointOfInterestService")
public class PointOfInterestServiceImpl implements PointOfInterestService {
	
	@Autowired
	private RPointOfInterest Rpoi;

	@Override
	public String getPoisJson(Set<PointOfInterest> POIs) {
		
		String JPois="[";
		
		if(!POIs.isEmpty()) {
			Iterator<PointOfInterest> pIt=POIs.iterator();
			for(int i=0; i<POIs.size(); i++) {
				
				PointOfInterest poi=pIt.next();
				JPois+="{\"foto\" : \""+poi.getRimage()+"\", \"the_geom\" : \""+poi.getPos()+"\"}";
				
				if(i<POIs.size()-1) {
					JPois+=",";
				}else {
					JPois+="]";
				}
			}
		}else
			JPois="[]";
		
		return JPois;
	}

	@Override
	public String getNextPoiJson(int id, String PosCrtl) {
		
		PointOfInterest poi=Rpoi.getNextPoi(id, PosCrtl);
		String JPOI=null;
		if(poi!=null)
			JPOI="[{ \"id\" : "+poi.getId()+", \"descrizione\" : \""+poi.getDescr()+"\", \"the_geom\" : \""+poi.getPos()+"\", \"foto\" : \""+poi.getRimage()+"\"}]";
		return JPOI;
	}
	
	@Override
	@Transactional
	public void SavePoi(PointOfInterest POI) {
		Rpoi.save(POI);
	}

}
