package com.CasaPiu.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.CasaPiu.Entity.SafePath;

public interface RSafePath extends JpaRepository<SafePath,Long> {
	
	@Autowired
	SafePath findById(int id);
	
	/*sfruttando spatial mysql si verifica che vi sia un intersezione tra: l'area individuata dal percorso sicuro, 
	 * ed un buffer circolare (con un raggio di circa 5m) centrato nella posizione dell'utente controllato*/
	@Query(value="SELECT ST_Intersects( ST_Buffer(ST_GeomFromText( ?1 ),0.000025),ST_Buffer(ST_GeomFromText(?2),0.00005) ) as isinspath", nativeQuery=true)
	int getIsInSPath(String spath, String PosCrtl);
}
